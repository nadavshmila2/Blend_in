package com.magshimim.jn.blend_in;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import net.alhazmy13.wordcloud.ColorTemplate;
import net.alhazmy13.wordcloud.WordCloud;
import net.alhazmy13.wordcloud.WordCloudView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class CommonWordsFragment extends Fragment {

        private static final String TAG = "CommonWordsFragment";
        private JSONObject data;
        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.common_words, container, false);
            try {
                data = new JSONObject(getArguments().getString("response"));
                data = data.getJSONObject("KeyWords");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Iterator<String> keys = data.keys();
            List<WordCloud> cloudEntries = new ArrayList<>();

            try {
                while(keys.hasNext())
                {
                    String currKey = keys.next();
                    cloudEntries.add(new WordCloud(currKey,Integer.parseInt(data.get(currKey).toString())));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            WordCloudView wordCloud = view.findViewById(R.id.keywordCloud);
            wordCloud.setDataSet(cloudEntries);
            wordCloud.setColors(ColorTemplate.COLORFUL_COLORS);
            wordCloud.notifyDataSetChanged();

            return view;
        }
}

