package com.magshimim.jn.blend_in;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class HistoryFragment extends Fragment {

    private static final String TAG = "HistoryFragment";

    List<String> listItems;
    ArrayAdapter<String> adapter;
    ListView listView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.history_fragment, container, false);

        listItems = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.HistoryArray)));

        listView = view.findViewById(R.id.listViewHistory);

        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,
                listItems);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            public void onItemClick(AdapterView<?>adapter,View v, int position, long id){
                MainComp.currentContext = getActivity();
                String selectedFromList = (listView.getItemAtPosition(position)).toString();
                MainComp.getInstance().sendSearch(selectedFromList);
            }
        });
        return view;
    }

   public void addItems(String toAdd)
   {
        listItems.add(toAdd);
        adapter.notifyDataSetChanged();
   }
}

