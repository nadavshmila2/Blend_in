package com.magshimim.jn.blend_in;

public interface INetworkHandler {

    //Function to handle a good response from the server (200 ok)
    void HandleSuccess(String response);

    //Function to handle a bad response from the server (404 not found, 403 unauthorized , etc)
    void HandleFailure(String response,int statusCode);
}
