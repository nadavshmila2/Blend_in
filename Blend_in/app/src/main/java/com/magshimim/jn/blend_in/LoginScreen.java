package com.magshimim.jn.blend_in;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import java.io.IOException;

public class LoginScreen extends AppCompatActivity implements OnEditorActionListener, INetworkHandler {
    private EditText email;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);
        email = findViewById(R.id.Client_Email);
        password = findViewById(R.id.Client_Password);
        password.setOnEditorActionListener(this);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onClickLogin(v);
            return true;
        }
        return false;
    }

    public void onClickLogin(View view) {
        String userEmail = email.getText().toString();
        String userPassword = password.getText().toString();
//
       if (userEmail.isEmpty() || userPassword.isEmpty()) {
            Toast.makeText(this, "Please enter an email and a password", Toast.LENGTH_SHORT).show();
            return;
        }


        NetworkManager nt;
        try {
            nt = new NetworkManager("http://62.219.161.166:2000/", this);
            nt.addParameter("Type", "Login");
            nt.addParameter("Email", userEmail);
            nt.addParameter("Password", userPassword);
            nt.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onClickRegister(View view) {
        Intent Register = new Intent(this, RegisterScreen.class);
        startActivity(Register);
    }


    public void HandleSuccess(String response) {
        Intent MainScreen = new Intent(this, UserInterface.class);
        MainScreen.putExtra("Token" ,response);
        startActivity(MainScreen);
    }


    public void HandleFailure(String response, int statusCode) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }

}
