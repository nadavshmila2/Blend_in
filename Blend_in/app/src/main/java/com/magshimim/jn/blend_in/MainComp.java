package com.magshimim.jn.blend_in;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.io.IOException;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.List;


public class MainComp extends Application implements INetworkHandler {

    public static List<String> listItems;
    public static String token;
    public static String query;
    public static Context currentContext;
    private static MainComp instance;


    protected MainComp() {
        // Exists only to defeat instantiation.
    }

    @Override
    public void onCreate() {
        listItems = new ArrayList<>();
        super.onCreate();
    }

    public static MainComp getInstance() {
        if(instance == null) {
            instance = new MainComp();
        }
        return instance;
    }

    public void addFavorite(String query, INetworkHandler handler) {
        listItems.add(query);
        sendRequest(query, "Favorite", handler);
    }


    public void sendSearch(String query)
    {
        sendRequest(query, "Search", MainComp.this);
    }

    public void sendSearch(String query, INetworkHandler handler)
    {
        sendRequest(query, "Search", handler);
    }

    public void sendRequest(String query, String type, INetworkHandler handler)
    {
        NetworkManager nt;
        try {
            nt = new NetworkManager("http://62.219.161.166:2000/", handler);
            nt.addParameter("Type", type);
            nt.addParameter("Query", query);
            nt.addParameter("Token",token);

            nt.execute();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void fetchData(INetworkHandler handler)
    {
        NetworkManager nt;
        try {
            nt = new NetworkManager("http://62.219.161.166:2000/", handler);
            nt.addParameter("Type", "Fetch");
            nt.addParameter("Token",token);

            nt.execute();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void HandleSuccess(String response) {
        Intent searchResults= new Intent(currentContext, SearchResults.class);
        searchResults.putExtra("response",response);
        searchResults.putExtra("query",query);
        currentContext.startActivity(searchResults);

    }


    public void HandleFailure(String response, int statusCode) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }

}
