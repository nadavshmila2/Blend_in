package com.magshimim.jn.blend_in;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;



public class NetworkManager extends AsyncTask<Void, Void, String> {
    private HttpURLConnection connection;
    private OutputStream outputStream;
    private HashMap<String, String> params = new HashMap<>();
    private INetworkHandler handler;
    @Override
    protected String doInBackground(Void... voids) {
        try {
            post();
            return getStringResponse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            if (connection.getResponseCode() == 200) {
                handler.HandleSuccess(result);
            } else {
                handler.HandleFailure(result,connection.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Network Fault", e.getMessage());
        }
        close();
    }

    public enum Method {
        POST
    }


    //"http://localhost:2000/server/"
    public NetworkManager(String url, INetworkHandler handler) throws IOException {
        this.handler = handler;
        connection = (HttpURLConnection) new URL(url).openConnection();
    }


    public void post() throws IOException {
        connection.setDoInput(true);
        connection.setRequestMethod(Method.POST.toString());
        connection.setDoOutput(true);
        outputStream = connection.getOutputStream();
        this.send();
    }

    public void addParameter(String key, String value) {
        this.params.put(key, value);
    }

    String getStringResponse() throws IOException {
        BufferedReader br;
        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK)
            br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        else
            br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
        StringBuilder response = new StringBuilder();
        for (String line; (line = br.readLine()) != null; ) response.append(line + "\n");
        return response.toString();
    }

    public void close() {
        if (null != connection)
            connection.disconnect();
    }

    private int send() throws IOException {
        int httpStatusCode;

        if (!this.params.isEmpty()) {
            this.sendData();
        }
        httpStatusCode = connection.getResponseCode();

        return httpStatusCode;
    }

    private void sendData() throws IOException {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append((result.length() > 0 ? "&" : "") + entry.getKey() + "=" + entry.getValue());//appends: key=value (for first param) OR &key=value(second and more)
        }
        sendData(result.toString());
    }

    private void sendData(String query) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
        writer.write(query);
        writer.close();
    }
}