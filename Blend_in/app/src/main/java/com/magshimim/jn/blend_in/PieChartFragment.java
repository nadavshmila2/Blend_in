package com.magshimim.jn.blend_in;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class PieChartFragment extends Fragment{

    private static final String TAG = "PieChartFragment";
    PieChart chart;
    private JSONObject data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pie_chart, container, false);
        chart = view.findViewById(R.id.chart);

        try {
            data = new JSONObject(getArguments().getString("response"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            setUpPieChart();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }
    private void setUpPieChart() throws JSONException {

        List<PieEntry> pieEntries = new ArrayList<>();
        JSONObject PieRelatedResultsAndStuff;

        PieRelatedResultsAndStuff = (JSONObject)data.get("Pie");

        Iterator<String> keys = PieRelatedResultsAndStuff.keys();

        try {
            while(keys.hasNext())
            {
                String currKey = keys.next();
                pieEntries.add(new PieEntry(Float.valueOf(PieRelatedResultsAndStuff.get(currKey).toString()),currKey));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PieDataSet dataSet = new PieDataSet(pieEntries,"The opinions for the query");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData data = new PieData(dataSet);
        chart.setData(data);
        chart.animateY(1000);
        chart.invalidate();
    }
}



