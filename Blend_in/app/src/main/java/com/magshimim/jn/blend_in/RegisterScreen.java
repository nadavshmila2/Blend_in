package com.magshimim.jn.blend_in;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;


import java.io.IOException;


public class RegisterScreen extends AppCompatActivity implements OnEditorActionListener, INetworkHandler{

    private EditText email;
    private EditText password;
    private EditText confirmPassword;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        email = findViewById(R.id.Client_Email);
        password = findViewById(R.id.Client_Password);
        confirmPassword = findViewById(R.id.Client_Password_confirm);
        confirmPassword.setOnEditorActionListener(this);

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onClick(v);
            return true;
        }
        return false;
    }

    public void onClick(View view){
        String userEmail = email.getText().toString();
        String userPassword = password.getText().toString();
        String userConfirmPassword = confirmPassword.getText().toString();
        if(userEmail.isEmpty() || userPassword.isEmpty() || userConfirmPassword.isEmpty())
        {
            Toast.makeText(this, "All fields must be entered in order to proceed", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!userPassword.equals(userConfirmPassword))
        {
            Toast.makeText(this, "Please make sure your password is confirmed correctly", Toast.LENGTH_SHORT).show();
            return;
        }
        NetworkManager networkManager;
        try {
            networkManager= new NetworkManager("http://62.219.161.166:2000/", this);
            networkManager.addParameter("Type", "Register");
            networkManager.addParameter("Email", userEmail);
            networkManager.addParameter("Password", userPassword);
            networkManager.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void HandleSuccess(String response) {
        Intent MainScreen = new Intent(this, UserInterface.class);
        MainScreen.putExtra("Token" ,response);
        startActivity(MainScreen);
    }


    public void HandleFailure(String response,int statusCode) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }


}


