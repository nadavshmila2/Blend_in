package com.magshimim.jn.blend_in;

import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;

import android.view.Window;
import android.widget.Toast;

public class SearchResults extends AppCompatActivity implements INetworkHandler {

    public Bundle data;
    private boolean clickedFAB;

    String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        query = getIntent().getStringExtra("query");
        setContentView(R.layout.activity_search_results);
        clickedFAB = false;

        data = new Bundle();
        data.putString("response", getIntent().getStringExtra("response"));
        final FloatingActionButton fab = findViewById(R.id.favorites);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!clickedFAB)
                {
                    fab.setImageResource(R.drawable.ic_star_gold_full_24dp);
                    MainComp.getInstance().addFavorite(query, SearchResults.this);
                }
                else
                {
                    fab.setImageResource(R.drawable.ic_star_border_gold_24dp);
                    MainComp.listItems.remove(query);
                }
            }
        });
        ViewPager mViewPager = findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        CommonWordsFragment cwf = new CommonWordsFragment();
        PieChartFragment pcf = new PieChartFragment();
        HashTagsFragment htf = new HashTagsFragment();
        cwf.setArguments(data);
        pcf.setArguments(data);
        htf.setArguments(data);
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(cwf, "keywords");
        adapter.addFragment(pcf, "stats");
        adapter.addFragment(htf, "hashtags");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }


    @Override
    public void HandleSuccess(String response) {
        Toast.makeText(this, "Item added to favorites", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void HandleFailure(String response, int statusCode) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }
}
