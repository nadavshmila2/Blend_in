package com.magshimim.jn.blend_in;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Window;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class UserInterface extends AppCompatActivity implements INetworkHandler {

    private static final String TAG = "UserInterface";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_user_interface);

        ViewPager mViewPager = findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        SearchView searchView = findViewById(R.id.searchView);

        MainComp.token = getIntent().getStringExtra("Token");

        MainComp.getInstance().fetchData(UserInterface.this);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                MainComp.currentContext = UserInterface.this;
                MainComp.query = s;
                HistoryFragment page = (HistoryFragment)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":" + "2");
                page.addItems(s);
                MainComp.getInstance().sendSearch(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                //Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }




    private void setupViewPager(ViewPager viewPager) {

        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new FavoritesFragment(), "Favorites");
        adapter.addFragment(new MainScreenFragment(), "MainScreen");
        adapter.addFragment(new HistoryFragment(), "History");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }


    private static Document parseXml(String str) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;

        builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(str)));

        return document;

    }

    @Override
    public void HandleSuccess(String response) {
        try {
            Document doc = parseXml(response);
            NodeList nList = doc.getElementsByTagName("history");
            ArrayList<String> historyItems = new ArrayList<>();
            //TODO: Finish xml parsing the data given from the server

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    historyItems.add(eElement.getTextContent());
                }

            }

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void HandleFailure(String response, int statusCode) {
//do add stuff
    }
}
