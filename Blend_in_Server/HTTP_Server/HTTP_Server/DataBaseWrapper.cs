﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Data;

namespace HTTP_Server
{

	public class DataBaseWrapper
	{
		private MySqlConnection connection;
		private string server;
		private string database;
		private string uid;
		private string password;

		/// <summary>
		/// Set all database related properties and secure a connection to the database
		/// </summary>
		public DataBaseWrapper()
		{
			server = "localhost";
			database = Properties.Resources.DataBaseName;
			uid = Properties.Resources.DataBaseUID;
			password = Properties.Resources.DataBasePassword;

			string connectionString = "SERVER=" + server + ";" + "DATABASE=" +
			database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

			connection = new MySqlConnection(connectionString);
		}

		/// <summary>
		/// Attempts to open a connection
		/// </summary>
		/// <returns>a string which is empty when successful, and an error message when unsuccessful</returns>
		private string OpenConnection()
		{
			try
			{
				if (connection.State != ConnectionState.Closed)
					connection.Close();
				connection.Open();
				return "";
			}
			catch (MySqlException ex)
			{
				//When handling errors, you can your application's response based 
				//on the error number.
				//The two most common error numbers when connecting are as follows:
				//0: Cannot connect to server.
				//1045: Invalid user name and/or password.
				switch (ex.Number)
				{
					case 0:
						return "Cannot connect to server.  Contact administrator";

					case 1045:
						return "Invalid username/password, please try again";

					default:
						return "Unidentified error. Error code was: " + ex.Number;
				}
			}
		}


		/// <summary>
		/// A function to search the database and retrieve the information of the user
		/// </summary>
		/// <param name="username">the email address of the user</param>
		/// <returns>A dictionary containing the username, password, and the salt of the user</returns>
		public Dictionary<string, string> GetUserInfo(string username)
		{
			string query = "SELECT * FROM accounts where username='" + username + "'";

			//Create a list to store the result
			Dictionary<string, string> users = new Dictionary<string, string>();

			//Open connection
			if (OpenConnection() == "")
			{
				//Create Command
				MySqlCommand cmd = new MySqlCommand(query, connection);
				//Create a data reader and Execute the command
				MySqlDataReader dataReader = cmd.ExecuteReader();

				DataTable schemaTable = dataReader.GetSchemaTable();

				//Read the data and store them in the list
				if (dataReader.Read())
				{
					for (int col = 0; col < dataReader.FieldCount; col++)
					{
						string fieldName = dataReader.GetName(col).ToString();
						if (dataReader[fieldName] == DBNull.Value)
							continue;
						if (fieldName == "token_expire")
						{
							users.Add(fieldName, dataReader.GetDateTime(fieldName).ToString());
							continue;
						}
						users.Add(fieldName, dataReader[fieldName].ToString());
					}
				}
				//close Data Reader
				dataReader.Close();

				//return list to be displayed
			}
			if (connection.State != ConnectionState.Closed)
				connection.Close();
			return users;
		}

		//TODO: CHECK IF THIS FUCKED SHIT UP
		/// <summary>
		/// Function to get the user data from the token
		/// </summary>
		/// <param name="token">the token the client got on login</param>
		/// <returns>the information of the user</returns>
		public Dictionary<string, string> GetUserFromToken(string token)
		{
			string query = "SELECT * FROM accounts where token='" + token + "'";

			//Create a list to store the result
			Dictionary<string, string> users = new Dictionary<string, string>();

			//Open connection
			if (OpenConnection() == "")
			{
				//Create Command
				MySqlCommand cmd = new MySqlCommand(query, connection);
				//Create a data reader and Execute the command
				MySqlDataReader dataReader = cmd.ExecuteReader();

				DataTable schemaTable = dataReader.GetSchemaTable();

				//Read the data and store them in the list
				if (dataReader.Read())
				{
					for (int col = 0; col < dataReader.FieldCount; col++)
					{
						string fieldName = dataReader.GetName(col).ToString();
						if (dataReader[fieldName] == DBNull.Value)
							continue;
						if (fieldName == "token_expire")
						{
							users.Add(fieldName, dataReader.GetDateTime(fieldName).ToString());
							continue;
						}
						users.Add(fieldName, dataReader[fieldName].ToString());
					}
				}
				//close Data Reader
				dataReader.Close();

				//return list to be displayed
			}
			if (connection.State != ConnectionState.Closed)
				connection.Close();
			return users;
		}

		/// <summary>
		/// A function to remove the account from the database
		/// </summary>
		/// <param name="username">the email of the user</param>
		/// <returns>an empty string when successful and an error message when unsuccessful</returns>
		public string RemoveAccount(string username)
		{
			string nonQuery = "DELETE FROM accounts WHERE username='" + username + "'";
			string connectionStatus = OpenConnection();
			if (connectionStatus == "")
			{
				//create command and assign the query and connection from the constructor
				MySqlCommand cmd = new MySqlCommand(nonQuery, connection);

				//Execute command
				cmd.ExecuteNonQuery();

				//close connection

			}
			if (connection.State != ConnectionState.Closed)
				connection.Close();
			return connectionStatus;
		}


		/// <summary>
		/// A function to add a search query item to the user's history
		/// </summary>
		/// <param name="query">the search query to add to the database</param>
		/// <param name="username">the email address of the user</param>
		/// <returns>an empty string when successful and an error message when unsuccessful</returns>
		public string AddHistoryItem(string query, string username)
		{
			string dbQuery = "INSERT IGNORE INTO history (username, query) VALUES('" + username + "', '" + query + "')";
			string connectionStatus = OpenConnection();

			//open connection
			if (connectionStatus == "")
			{
				//create command and assign the query and connection from the constructor
				MySqlCommand cmd = new MySqlCommand(dbQuery, connection);

				cmd.ExecuteNonQuery();


			}
			if (connection.State != ConnectionState.Closed)
				connection.Close();
			return connectionStatus;
		}


		/// <summary>
		/// A function to add a search query item to the user's history
		/// </summary>
		/// <param name="query">the search query to add to the database</param>
		/// <param name="username">the email address of the user</param>
		/// <returns>an empty string when successful and an error message when unsuccessful</returns>
		public string AddToken(string username, string token)
		{
			string expire_date = (DateTime.Now.AddDays(9)).ToString("yyyy-MM-dd HH:mm:ss");
			string dbQuery = "UPDATE accounts set token='" + token + "', token_expire='" + expire_date + "' WHERE username='" + username + "'";
			string connectionStatus = OpenConnection();

			//open connection
			if (connectionStatus == "")
			{
				//create command and assign the query and connection from the constructor
				MySqlCommand cmd = new MySqlCommand(dbQuery, connection);

				cmd.ExecuteNonQuery();


			}
			if (connection.State != ConnectionState.Closed)
				connection.Close();
			return connectionStatus;
		}


		/// <summary>
		/// A function to add a search query item to the user's favorites
		/// </summary>
		/// <param name="query">the search query to add to the database</param>
		/// <param name="username">the email address of the user</param>
		/// <returns>an empty string when successful and an error message when unsuccessful</returns>
		public string AddFavoriteItem(string query, string username)
		{
			string dbQuery = "INSERT IGNORE INTO favorite (username, query) VALUES('" + username + "', '" + query + "')";
			string connectionStatus = OpenConnection();

			//open connection
			if (connectionStatus == "")
			{
				//create command and assign the query and connection from the constructor
				MySqlCommand cmd = new MySqlCommand(dbQuery, connection);

				//Execute command
				cmd.ExecuteNonQuery();

				//close connection

			}
			if (connection.State != ConnectionState.Closed)
				connection.Close();
			return connectionStatus;
		}

		/// <summary>
		/// A function to access the database and retrieve the user's favorite searches and results
		/// </summary>
		/// <param name="username">the email address of the user in question</param>
		/// <returns>a dictionary containing the user's favorites information</returns>
		public Dictionary<string, List<string>> GetFavoritesByUser(string username)
		{
			return GetDataByUser(username, "favorite");
		}

		/// <summary>
		/// A function to access the database and retrieve the user's past searches (not including the results)
		/// </summary>
		/// <param name="username">the email address of the user in question</param>
		/// <returns>a dictionary containing all of the user's queries</returns>
		public Dictionary<string, List<string>> GetHistoryByUser(string username)
		{
			return GetDataByUser(username, "history");
		}

		/// <summary>
		/// A function to access the database and get all information in the table relating to the user
		/// </summary>
		/// <param name="username">the email address of the user</param>
		/// <param name="table">the table to look in</param>
		/// <returns></returns>
		private Dictionary<string, List<string>> GetDataByUser(string username, string table)
		{
			string query = "SELECT * FROM " + table + " where username='" + username + "'";

			//Create a list to store the result
			Dictionary<string, List<string>> users = new Dictionary<string, List<string>>();

			//Open connection
			if (OpenConnection() == "")
			{
				//Create Command
				MySqlCommand cmd = new MySqlCommand(query, connection);
				//Create a data reader and Execute the command
				MySqlDataReader dataReader = cmd.ExecuteReader();

				DataTable schemaTable = dataReader.GetSchemaTable();

				//Read the data and store them in the list
				while (dataReader.Read())
				{
					for (int col = 0; col < dataReader.FieldCount; col++)
					{
						string fieldName = dataReader.GetName(col).ToString();

						if (fieldName == "username")
							continue;

						if (users.TryGetValue(fieldName, out List<string> vals))
						{
							vals.Add(dataReader[fieldName].ToString());

						}
						else
						{
							vals = new List<string>
							{
								dataReader[fieldName].ToString()
							};
						}
						users[fieldName] = vals;

					}
				}
				//close Data Reader
				dataReader.Close();

				//close Connection

				//return list to be displayed
			}
			if (connection.State != ConnectionState.Closed)
				connection.Close();
			return users;
		}


		/// <summary>
		/// A function to add an account to the database
		/// </summary>
		/// <param name="username">the email address</param>
		/// <param name="password">the hashed password</param>
		/// <param name="salt">the salt used with the password</param>
		/// <returns></returns>
		public string AddAccount(string username, string password, string salt)
		{
			string query = "INSERT INTO accounts (username, password, salt) VALUES('" + username + "', '" + password + "', '" + salt + "')";
			string connectionStatus = OpenConnection();

			//open connection
			if (connectionStatus == "")
			{
				//create command and assign the query and connection from the constructor
				MySqlCommand cmd = new MySqlCommand(query, connection);

				//Execute command
				cmd.ExecuteNonQuery();

				//close connection

			}
			if (connection.State != ConnectionState.Closed)
				connection.Close();
			return connectionStatus;

		}
	}


	/// <summary>
	/// A simple exception for DataBaseWrapper related exceptions 
	/// </summary>
	public class DataBaseWrapperException : Exception
	{
		public DataBaseWrapperException(string message)
			: base(message)
		{
		}
	}
}
