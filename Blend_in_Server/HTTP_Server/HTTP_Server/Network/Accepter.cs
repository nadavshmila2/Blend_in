﻿using System;
using System.Collections.Generic;
using System.Net;

namespace HTTP_Server
{
	public class Accepter
	{
		//The object used to interface with the database
		private DataBaseWrapper dbWrapper;

		//The listener object to get the requests
		private HttpListener listener;

		private List<IAsyncResult> handlers;

		//The amount of listener threads
		private int listeners;

		//The function used to handle each request sent by the context
		public delegate Tuple<string, HttpStatusCode> RequestHandler(Dictionary<string, string> parameters);


		public Accepter(DataBaseWrapper dbWrapper, string Prefix, int listeners = 1)
		{

			this.listeners = listeners;

			this.dbWrapper = dbWrapper;

			//Create a new listener and set listnening to all what is specified by Prefix
			listener = new HttpListener();
			listener.Prefixes.Add(Prefix);
		}


		public void Close()
		{
			listener.Stop();
			RequestWorker.Stop();
			foreach (IAsyncResult handler in handlers)
			{
				handler.AsyncWaitHandle.WaitOne();
			}
		}


		/// <summary>
		/// Start the listener and resolve all requests using requestHandler.
		/// </summary>
		/// <param name="requestHandler">The function that will handle the request</param>
		public void StartListen()
		{
			listener.Start();
			Tuple<HttpListener, DataBaseWrapper> state = new Tuple<HttpListener, DataBaseWrapper>(listener, dbWrapper);

			handlers = new List<IAsyncResult>(listeners);

			for (int i = 0; i < listeners; i++)
			{
				Console.WriteLine("Starting handler: " + (i+1));


				handlers.Add(listener.BeginGetContext(new AsyncCallback(RequestWorker.Handle), state));
				
			}

		}
	}
}
