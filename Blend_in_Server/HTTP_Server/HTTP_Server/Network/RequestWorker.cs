﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace HTTP_Server
{
	public class RequestWorker
	{

		private static bool stop = false;

		public static void Handle(IAsyncResult ar)
		{
			//Get the parameters from the acccepter class
			Tuple<HttpListener, DataBaseWrapper> pararmeters = (Tuple<HttpListener, DataBaseWrapper>)ar.AsyncState;
			HttpListener listener = pararmeters.Item1;

			DataBaseWrapper dbWrapper = pararmeters.Item2;

			try
			{

				HttpListenerContext context = listener.EndGetContext(ar);

				ProcessRequest processor = new ProcessRequest(dbWrapper);

				Tuple<string, HttpStatusCode> response = processor.SelectFunction(ReadRequest(context));

				//Create and send the response back to the context

				CreateResponse(response, context);

				Console.WriteLine("Message sent at " + DateTime.Now.ToLongTimeString() + "\n\n\n");

			}
			catch (HttpListenerException)
			{ }
			finally
			{
				if (!stop)
					listener.BeginGetContext(new AsyncCallback(Handle), pararmeters);
			}
		}

		public static void Stop()
		{
			stop = true;
		}





		/// <summary>
		/// Creates a response from the string given and sends it the context.
		/// </summary>
		/// <param name="response">The response message and status code</param>
		public static void CreateResponse(Tuple<string, HttpStatusCode> response, HttpListenerContext context)
		{
			Console.WriteLine(response.Item1);
			//Prepare the response for sending
			context.Response.ContentLength64 = Encoding.UTF8.GetByteCount(response.Item1);
			context.Response.StatusCode = (int)response.Item2;
			if (IsXML(response.Item1))
				context.Response.ContentType = "text/xml; encoding='utf-8'";
			//Send the entire response to the context
			using (Stream stream = context.Response.OutputStream)
			{
				using (StreamWriter writer = new StreamWriter(stream))
				{
					writer.Write(response.Item1);
				}
			}
		}

		/// <summary>
		/// Function to check whether or not the string given is of xml type
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		private static bool IsXML(string str)
		{
			if (!string.IsNullOrEmpty(str) && str.TrimStart().StartsWith("<"))
			{
				try
				{
					new XmlDocument().LoadXml(str);
					return true;

				}
				catch (XmlException)
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Reads a request from the context.
		/// </summary>
		/// <returns>The parameters sent with the POST/GET request in a Dictionary</returns>
		public static Dictionary<string, string> ReadRequest(HttpListenerContext context)
		{

			HttpListenerRequest request = context.Request;

			//The final processed parameters
			Dictionary<string, string> processedParams = new Dictionary<string, string>();

			//Read the raw data from the context and encode it
			string data;
			using (Stream body = request.InputStream) // here we have data
			{
				using (StreamReader reader = new StreamReader(body, request.ContentEncoding))
				{
					data = reader.ReadToEnd();
					data = Uri.UnescapeDataString(data);
				}
			}

			//Split the raw data into query parameters (key=value)
			string[] rawParams = data.Split('&');
			foreach (string rawParam in rawParams)
			{
				if (rawParam.Equals(""))
					continue;

				string[] keyValPair = rawParam.Split('=');
				//Split the query parameters into a dictionary
				processedParams.Add(keyValPair[0], keyValPair[1]);
			}

			//Because token is in base64 and padding is '==' the algorithm splits it, to avoid it we manually add '==' in the end
			if (processedParams.TryGetValue("Token", out string token))
			{
				token += "==";
				processedParams["Token"] = token;
			}

			return processedParams;
		}
	}
}
