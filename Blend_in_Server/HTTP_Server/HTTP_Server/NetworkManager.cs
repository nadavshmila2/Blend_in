﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

namespace HTTP_Server
{
	class NetworkManager
	{

		IAuthenticatedUser user;

		public NetworkManager()
		{
			Auth.SetUserCredentials(Properties.Resources.ConsumerKey, Properties.Resources.ConsumerKeySecret, Properties.Resources.AccessToken, Properties.Resources.AccessTokenSecret);
			user = User.GetAuthenticatedUser();
			SearchTweet("Hello");
		}


		public IEnumerable<string> SearchTweet(string query, GeoCode geoCode = null)
		{

			List<string> tweetsRes = new List<string>();
			var searchParams = new SearchTweetsParameters(query)
			{
				GeoCode = geoCode
			};

			var tweets = Search.SearchTweets(searchParams);
			foreach (var tweet in tweets)
			{
				string text = tweet.Text;
				text = text.Substring(0, 3).Equals("RT ") ? text.Substring(3) : text;
				Console.WriteLine(text);
				tweetsRes.Add(tweet.Text);
			}
			return tweetsRes;

		}


		public void PublishTweet(string tweet)
		{
			Tweet.PublishTweet(tweet);
		}

		/*
				public IEnumerable<string> GetRecentTweets(int amountOfTweets)
				{
					return Timeline.GetUserTimeline(user);

				}
		*/
	}
}
