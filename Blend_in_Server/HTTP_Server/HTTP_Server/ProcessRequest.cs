﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace HTTP_Server
{
	public class ProcessRequest
	{

		DataBaseWrapper dbWrapper;

		public ProcessRequest(DataBaseWrapper dataBaseWrapper)
		{
			dbWrapper = dataBaseWrapper;
		}

		#region Processing that isn't related to actual networking

		/// <summary>
		/// Registers the account.
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public Tuple<string, HttpStatusCode> Register(Dictionary<string, string> parameters)
		{
			string password, email, salt;
			try
			{
				Tuple<string, string> passwordParts = HashCredentials(parameters["Password"]);
				password = passwordParts.Item1;
				salt = passwordParts.Item2;
				email = parameters["Email"];

			}
			catch (KeyNotFoundException)
			{
				return new Tuple<string, HttpStatusCode>("Password or username were not sent correctly, please enter all fields and try again", HttpStatusCode.BadRequest);
			}

			var ret = Validator.ValidCredentials(password, email);

			if (ret == Validator.ValidatorReturn.EMAIL)
				return new Tuple<string, HttpStatusCode>("Please enter a valid email", HttpStatusCode.Unauthorized);

			else if (EmailExists(email))
				return new Tuple<string, HttpStatusCode>("Email has already been used", HttpStatusCode.Conflict);

			else if (ret == Validator.ValidatorReturn.PASS)
				return new Tuple<string, HttpStatusCode>("Password must be more than 8 characters long, and have at least one\nlowercase letter, uppercase letter, special character and number", HttpStatusCode.Unauthorized);

			string accountInfo = dbWrapper.AddAccount(email, password, salt);

			if (accountInfo.Equals(""))
			{
				string token = GenerateString(40);
				dbWrapper.AddToken(email, token);
				return new Tuple<string, HttpStatusCode>(token, HttpStatusCode.OK);
			}

			return new Tuple<string, HttpStatusCode>("Error has occured please register again", HttpStatusCode.Conflict);
		}



		public Tuple<string, HttpStatusCode> Fetch(Dictionary<string, string> parameters)
		{
			string username = GetUsernameFromToken(parameters["Token"]);

			if (username == "")
			{
				return new Tuple<string, HttpStatusCode>("Please login again", HttpStatusCode.Unauthorized);
			}

			try
			{
				//Make an xml document from the results of the database 
				//(Dictionary of data from the table -> XElement)
				XDocument doc = new XDocument(new XElement("body",
					DataToXml(dbWrapper.GetHistoryByUser(username), "history"),
					DataToXml(dbWrapper.GetFavoritesByUser(username), "favorite")));
				



				return new Tuple<string, HttpStatusCode>(doc.ToString(), HttpStatusCode.OK);
			}
			catch (KeyNotFoundException)
			{
				return new Tuple<string, HttpStatusCode>("Missing information in user's data, failed to get past searches and favorites", HttpStatusCode.BadRequest);
			}
		}

		/// <summary>
		/// Parses the data recieved from the database into xml to stage for sending
		/// </summary>
		/// <param name="list">the data that is being parsed</param>
		/// <param name="name">the name of the root node</param>
		/// <returns>an XElement containing a Node with the name and a list of child nodes called "item" 
		/// containing the data inside the dictionary's List<string>
		/// </returns>
		private XElement DataToXml(Dictionary<string, List<string>> list, string name)
		{
			XElement root = new XElement(name);
			if (list.Count > 0)
			{
				foreach (var item in list["query"])
				{
					root.Add(new XElement("item", item));
				}
			}
			return root;
		}

		/// <summary>
		/// Ret
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public Tuple<string, HttpStatusCode> Favorite(Dictionary<string, string> parameters)
		{
			string username = GetUsernameFromToken(parameters["Token"]);

			if (username == "")
			{
				return new Tuple<string, HttpStatusCode>("Please login again", HttpStatusCode.Unauthorized);
			}

			try
			{
				dbWrapper.AddFavoriteItem(parameters["Query"], username);
				return new Tuple<string, HttpStatusCode>("Operation successful", HttpStatusCode.OK);
			}
			catch (KeyNotFoundException)
			{
				return new Tuple<string, HttpStatusCode>("Query was not sent correctly, please enter all fields and try again", HttpStatusCode.BadRequest);
			}
		}

		/// <summary>
		/// Gets the user from the token and makes sure it is stil valid
		/// </summary>
		/// <param name="token"></param>
		/// <returns></returns>
		private string GetUsernameFromToken(string token)
		{
			var users = dbWrapper.GetUserFromToken(token);

			try
			{
				DateTime.TryParse(users["token_expire"], out DateTime sqlDate);

				if (DateTime.Compare(new SqlDateTime(sqlDate).Value, (DateTime.Now)) <= 0)
				{
					return "";
				}

				return users["username"];
			}
			catch (KeyNotFoundException)
			{
				return "";
			}
		}


		/// <summary>
		/// Activates a python script to get tweets based on a search qeury
		/// </summary>
		/// <param name="parameters">A dictionary containing the Query parameter</param>
		/// <returns></returns>
		public Tuple<string, HttpStatusCode> Search(Dictionary<string, string> parameters)
		{
			try
			{
				string username = GetUsernameFromToken(parameters["Token"]);

				if (username == "")
				{
					return new Tuple<string, HttpStatusCode>("Please login again", HttpStatusCode.Unauthorized);
				}

				string pythonLoc = @"..\PythonScript.py";//Python script location
				ProcessStartInfo start = new ProcessStartInfo
				{
					FileName = "python.exe",
					Arguments = string.Format("\"{0}\" \"{1}\"", pythonLoc, parameters["Query"]),//Cmd arguments (path, query)
					UseShellExecute = false,
					RedirectStandardOutput = true//Redirect the stdout to collect the tweets' information
				};

				Console.WriteLine(parameters["Query"]);

				//Open a reader and collect all information the python script gathered
				using (Process process = Process.Start(start))
				{
					using (StreamReader reader = process.StandardOutput)
					{
						string result = reader.ReadToEnd();
						//Return the information to the client

						//Insert the new query into the history, it's done in async as it shouldn't interrupt the client's process
						Task.Run(() => dbWrapper.AddHistoryItem(parameters["Query"], username));

						return new Tuple<string, HttpStatusCode>(result, HttpStatusCode.OK);

					}
				}
			}
			catch (KeyNotFoundException)
			{
				return new Tuple<string, HttpStatusCode>("Query was not sent correctly, please enter all fields and try again", HttpStatusCode.BadRequest);
			}
		}


		/// <summary>
		/// Attempts to login using the credentials sent.
		/// </summary>
		/// <param name="parameters">A dictionary containing 2 parameters, The password and the email</param>
		/// <returns></returns>
		public Tuple<string, HttpStatusCode> Login(Dictionary<string, string> parameters)
		{
			try
			{
				string password = parameters["Password"], email = parameters["Email"], salt;

				if (!EmailExists(email))
					return new Tuple<string, HttpStatusCode>("username doesn't exist", HttpStatusCode.Unauthorized);

				Dictionary<string, string> credentials = dbWrapper.GetUserInfo(email);

				salt = credentials["salt"];
				if (string.IsNullOrWhiteSpace(salt))
				{
					dbWrapper.RemoveAccount(email);
					return new Tuple<string, HttpStatusCode>("username couldn't be checked, user deleted. Register again", HttpStatusCode.InternalServerError);
				}
				password = HashCredentials(password, salt);

				if (!credentials["password"].Equals(password))
					return new Tuple<string, HttpStatusCode>("password doesn't match username", HttpStatusCode.Unauthorized);

				string token = GenerateString(40);
				dbWrapper.AddToken(email, token);
				//Return a new Token for the user to use
				return new Tuple<string, HttpStatusCode>(token, HttpStatusCode.OK);
			}
			catch (KeyNotFoundException)
			{
				return new Tuple<string, HttpStatusCode>("Password or username were not sent correctly, please enter all fields and try again", HttpStatusCode.BadRequest);
			}
		}


		/// <summary>
		/// Check with the database if the email exists as a username in the database
		/// </summary>
		/// <param name="email">the email to check</param>
		/// <returns>True if it exists
		///			 False otherwise</returns>
		private bool EmailExists(string email)
		{
			Dictionary<string, string> users = dbWrapper.GetUserInfo(email);
			return users.TryGetValue("username", out string storedEmail) && storedEmail.Equals(email);
		}



		/// <summary>
		/// Select the function to execute based on the request the client sent ( register, login ).
		/// </summary>
		/// <param name="request">The parameters the client sent</param>
		/// <returns>the message to send to the client with the status code</returns>
		public Tuple<string, HttpStatusCode> SelectFunction(Dictionary<string, string> request)
		{
			Tuple<string, HttpStatusCode> response;
			try
			{
				MethodInfo method = GetType().GetMethod(request["Type"]);
				response = (Tuple<string, HttpStatusCode>)method.Invoke(this, new Object[] { request });
			}
			catch (NullReferenceException)
			{
				response = new Tuple<string, HttpStatusCode>("Request type is wrong, report this to the developer", HttpStatusCode.BadRequest);
			}

			catch (KeyNotFoundException)
			{
				response = new Tuple<string, HttpStatusCode>("No request type found, report this to the developers", HttpStatusCode.BadRequest);
			}
			return response;
		}


		/// <summary>
		/// Create a random salt and hash the password
		/// </summary>
		/// <param name="password">The password</param>
		/// <returns>The hashed passowrd and the salt</returns>
		public Tuple<string, string> HashCredentials(string password)
		{
			string salt;
			salt = GenerateString(30);

			string hashedPassword = HashCredentials(password, salt);

			return new Tuple<string, string>(hashedPassword, salt);
		}


		/// <summary>
		/// Hash the password using a premade salt
		/// </summary>
		/// <param name="password">The plain text password</param>
		/// <param name="salt">A pregenerated salt to use</param>
		/// <returns>The hashed password</returns>
		public string HashCredentials(string password, string salt)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(password + salt);
			SHA256Managed hashedString = new SHA256Managed();
			return Convert.ToBase64String(hashedString.ComputeHash(bytes));
		}

		/// <summary>
		/// Create a random salt
		/// </summary>
		/// <param name="size">The size of the salt in bytes</param>
		/// <returns></returns>
		public static string GenerateString(int size)
		{
			var range = new RNGCryptoServiceProvider();
			var buffer = new byte[size];
			range.GetBytes(buffer);
			return Convert.ToBase64String(buffer);
		}

		#endregion
	}
}
