﻿using System;

namespace HTTP_Server
{
	public class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				DataBaseWrapper dbWrapper = new DataBaseWrapper();
				
				//Create an Accepter and start listening
				Accepter accepter = new Accepter(dbWrapper, "http://*:2000/", 10);
				accepter.StartListen();

				Console.ReadLine();

				accepter.Close();
			}
			catch(DataBaseWrapperException e)
			{
				//The data base wrapper has failed to connect to the database (fatal error)
				Console.WriteLine("DataBase could not be started.");
				Console.WriteLine(e);
			}
		}
	}
}
