﻿using System;
using System.Text.RegularExpressions;

namespace HTTP_Server
{
	public class Validator
	{

		public enum ValidatorReturn {
			GOOD,
			EMAIL,
			PASS
			
		}

		private static bool ValidEmail(string email)
		{
			//official email vertification from msdn (https://docs.microsoft.com/en-us/dotnet/standard/base-types/how-to-verify-that-strings-are-in-valid-email-format)
			return Regex.IsMatch(email, 
				@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
				@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
				RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
		}

		private static bool ValidPassword(string password)
		{
			return !string.IsNullOrWhiteSpace(password) &&//Not empty
				password.Length >= 8 &&//Longer than 7 characters
				Regex.IsMatch(password, @"[0-9]+") &&//Contains a number
				Regex.IsMatch(password, @"[A-Z]+") &&//Contains an uppercase
				Regex.IsMatch(password, @"[a-z]+");//Contains a lowercase
		}


		/// <summary>
		/// A function to verify that both fields are meeting the requirements
		/// </summary>
		/// <param name="password">the password</param>
		/// <param name="email">the email address</param>
		/// <returns>EMAIL - the email was incorrect
		///			 PASS - the password was incorrect
		///			 GOOD - all fields are correct</returns>
		public static ValidatorReturn ValidCredentials(string password, string email)
		{
			Console.WriteLine("Password: " + password + "\nEmail: " + email);
			if(!ValidEmail(email))//Check email validity
				return ValidatorReturn.EMAIL;//raise email flag if incorrect
	
			if (!ValidPassword(password))//Check email validity
				return ValidatorReturn.PASS;//raise password flag if incorrect

			return ValidatorReturn.GOOD;
		}
	}
}
