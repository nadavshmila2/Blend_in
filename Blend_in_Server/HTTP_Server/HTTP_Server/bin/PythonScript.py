import re
import tweepy
from tweepy import OAuthHandler
from textblob import TextBlob
import nltk
import sys
import json
from collections import Counter

class TwitterClient(object):
	'''
	Generic Twitter Class for sentiment analysis.
	'''

	def __init__(self):
		'''
		Class constructor or initialization method.
		'''
		# keys and tokens from the Twitter Dev Console
		consumer_key = 'QoJWu6OaiS2jolHZfredj8Xvx'
		consumer_secret = 'MDPpzsnnLzWYk5l6g1J4CrSLwnyKDAlXUkqv4QdMHxsSPWgeLh'
		access_token = '893490594085052416-bqXsYw9f8JQ2jJlDAMYscwGFGJKgUdZ'
		access_token_secret = 'PAYQxy8cHH82bIV8hh5fS65Qy29aON9iULGR8ge4xz2H5'

		# attempt authentication
		try:
			# create OAuthHandler object
			self.auth = OAuthHandler(consumer_key, consumer_secret)
			# set access token and secret
			self.auth.set_access_token(access_token, access_token_secret)
			# create tweepy API object to fetch tweets
			self.api = tweepy.API(self.auth)
		except:
			print("Error: Authentication Failed")

	def clean_tweet(self, tweet):
		'''
		Utility function to clean tweet text by removing links, special characters
		using simple regex statements.
		'''
		return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])", " ", re.sub(r"https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+"," ", tweet)).split()).lower()

	def get_tweet_sentiment(self, tweet):
		'''
		Utility function to classify sentiment of passed tweet
		using textblob's sentiment method
		'''
		# create TextBlob object of passed tweet text
		analysis = TextBlob(self.clean_tweet(tweet))
		# set sentiment
		if analysis.sentiment.polarity > 0:
			return 'positive'
		elif analysis.sentiment.polarity == 0:
			return 'neutral'
		else:
			return 'negative'

	def get_tweets(self, query, count=10):
		'''
		Main function to fetch tweets and parse them.
		'''
		# empty list to store parsed tweets
		tweets = []

		try:
			# call twitter api to fetch tweets
			fetched_tweets = self.api.search(q=query, count=count)
			# parsing tweets one by one
			for tweet in fetched_tweets:
				# empty dictionary to store required params of a tweet
				parsed_tweet = {}

				# saving text of tweet
				parsed_tweet['text'] = tweet.text
				# saving sentiment of tweet
				parsed_tweet['sentiment'] = self.get_tweet_sentiment(tweet.text)

				# appending parsed tweet to tweets list
				if tweet.retweet_count > 0:
					# if tweet has retweets, ensure that it is appended only once
					if parsed_tweet not in tweets:
						tweets.append(parsed_tweet)
				else:
					tweets.append(parsed_tweet)

			# return parsed tweets
			return tweets

		except tweepy.TweepError as e:
			# print error (if any)
			print("Error : " + str(e))


def get_common_hashtags(tweets):
	print(tweets)
			
			
			
			
def pie(tweets):
	
	# picking positive tweets from tweets
	ptweets = [tweet for tweet in tweets if tweet['sentiment'] == 'positive']
	
	# picking negative tweets from tweets
	ntweets = [tweet for tweet in tweets if tweet['sentiment'] == 'negative']
	
	
	results = {
		"Positive": (100 * len(ptweets) / len(tweets)),
		"Negative": (100 * len(ntweets) / len(tweets)),
		"Neutral": (100 * (len(tweets) - len(ntweets) - len(ptweets)) / len(tweets))
	}
	
	return results
	
	
	
def keywords(tweets, api, query):
	all_tweets = ""
	all_tweets = [all_tweets + api.clean_tweet(tweet['text']) for tweet in tweets]
	
	
	all_words = ' '.join(all_tweets)
	all_words = nltk.word_tokenize(all_words)
	all_words = [word for word in all_words if word != 'rt' and word not in query.split(' ')]

	
	all_words = nltk.FreqDist(all_words)
	
	frequent_words = all_words.most_common(40)
	
	frequent_words = dict((x,y) for x,y in frequent_words)
	
	return frequent_words
	
	

def hashtags(api, query):
	all_tweets = api.api.search(q=query, count=200)

	all_hashtags = []
	
	all_hashtags = [all_hashtags + clean_tweet(tweet._json) for tweet in all_tweets if clean_tweet(tweet._json) != []]
	
	all_hashtags = [item for sublist in all_hashtags for item in sublist]

	all_hashtags = Counter(all_hashtags)
	return all_hashtags
	


def clean_tweet(tweet):
	all_hashtags = tweet['entities']['hashtags']
	res = []
	for hashtag in all_hashtags:			
		res += [hashtag['text']]

	return res
	
def main(query):

	
	# creating object of TwitterClient Class
	api = TwitterClient()
	query = query.lower()
	# calling function to get tweets
	tweets = api.get_tweets(query=query, count=200)



	
	results = {
		"Pie": pie(tweets),
		"KeyWords": keywords(tweets, api, query),
		"CommonHashTags": hashtags(api,query)
	}
		
	print(json.dumps(results))


	
	
	


if __name__ == "__main__":
	# calling main function
	if len(sys.argv) > 1:
		main(sys.argv[1])
		
